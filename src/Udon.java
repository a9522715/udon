import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Udon {
    private JPanel root;
    private JButton kitsuneUdonButton;
    private JButton curryUdonButton;
    private JButton shurinpTempura;
    private JButton squidTempuraButton;
    private JButton chikuwaButton;
    private JButton nikuUdonButton;
    private JTextArea orderedItemsList;
    private JLabel totalArea;
    private JButton CheckOutBottn;
    int total=0;


    public static void main(String[] args) {
        JFrame frame = new JFrame("Udon");
        frame.setContentPane(new Udon().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String foodName,int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+foodName+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + foodName +" "+ price +"yen"+"\n");
            JOptionPane.showMessageDialog(null,"Order for "+foodName+" recieved.");
            total+=price;
            totalArea.setText("total  "+ total +"yen");
            if (foodName.equals("kitsuneUdon") || foodName.equals("nikuUdon") || foodName.equals("curryUdon")) {
                toppingShurinpTempura();
            }
        }

    }

    void toppingShurinpTempura(){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "How about some shrimp tempura?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                new ImageIcon(this.getClass().getResource("picture/ShurinpTempura.jpg"))
        );

        if(confirmation==0){
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText +  "shurinpTempura "+ "250 yen"+"\n");
            JOptionPane.showMessageDialog(null,"Order for shurinpTempura recieved.");
            total+=250;
            totalArea.setText("total  "+ total +"yen");
        }

    }

    public Udon() {
        kitsuneUdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int price=600;

                order("kitsuneUdon",price);
            }
        });
        kitsuneUdonButton.setIcon(new ImageIcon(
                this.getClass().getResource("picture/KitsuneUdon.jpg")
        ));

        nikuUdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int price=750;

                order("nikuUdon",price);
            }
        });
        nikuUdonButton.setIcon(new ImageIcon(
                this.getClass().getResource("picture/NikuUdon.jpg")
        ));

        curryUdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int price=800;

                order("curryUdon",price);
            }
        });
        curryUdonButton.setIcon(new ImageIcon(
                this.getClass().getResource("picture/CurryUdon.jpg")
        ));

        shurinpTempura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int nedann=250;

                order("shurinpTempura",nedann);
            }
        });
        shurinpTempura.setIcon(new ImageIcon(
                this.getClass().getResource("picture/ShurinpTempura.jpg")
        ));

        squidTempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int price=200;

                order("squidTempura",price);
            }
        });
        squidTempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("picture/SquidTempura.jpg")
        ));

        chikuwaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int price=150;

                order("chikuwa",price);
            }
        });
        chikuwaButton.setIcon(new ImageIcon(
                this.getClass().getResource("picture/Chikuwa.jpg")
        ));

        CheckOutBottn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        CheckOutBottn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout? ",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is "+ total +" yen.");
                    orderedItemsList.setText("");
                    total=0;
                    totalArea.setText("total  "+total+"yen");
                }
            }
        });
    }
}
